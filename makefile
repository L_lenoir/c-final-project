all:
	gcc vetement.c -o vetement
	gcc statistik_nouveau.c -o statistik_nouveau
	gcc cabine.c -o cabine
	gcc client.c -o client

clean:
	rm -f vetement statistik_nouveau close_semaphore
	rm -f cabine client
	rm -f /dev/shm/*vetement*