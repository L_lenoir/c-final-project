#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <time.h>
#include <wait.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <semaphore.h>

#define VETEMENT 20
#define TAILLE 3
#define NBR_VETEMENT 5

int createClient(int nbVetement)
{

}

int watch_vetement()
{
	sem_t *magasin[VETEMENT][TAILLE];

	for (int i = 0; i < VETEMENT; i++)
	{
		for (int j = 0; j < TAILLE; j++)
		{
			char semaphore_name[50];
			sprintf(semaphore_name, "/vetement_%d_%d", i, j);
			magasin[i][j] = sem_open(semaphore_name, 0);

			if (magasin[i][j] == SEM_FAILED) {
				perror("Erreur lors de la création de la sémaphore nommée");
				exit(EXIT_FAILURE);
			}

			int nbr_dispo;
			if (sem_getvalue(magasin[i][j], &nbr_dispo) == -1) {
				perror("Erreur lors de la lecture de la valeur de la sémaphore");
				exit(EXIT_FAILURE);
			}
			printf("Nombre de vetement %d taille %d diponible : %d\n", i, j, nbr_dispo);
		}
	}
	printf("-----------------------\n");
	return 0;
}

int main()
{
	while(1)
	{
		sleep(2);
		watch_vetement();
	}
}