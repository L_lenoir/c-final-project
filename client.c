#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <time.h>
#include <wait.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <semaphore.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/stat.h>

#define VETEMENT 20
#define TAILLE 3
#define FIFO_NAME "/tmp/file_attente"

struct vetement
{
    int num;
    int taille;
};

int client(int nbrvetement)
{
    // Initialisation
    if (nbrvetement > 4 || nbrvetement < 0)
    {
        perror("NBR VETEMENT invalide (0 < nbrvetement < 5)");
        exit(1);
    }

    struct vetement table_vetements[nbrvetement];
    for (int i = 0; i <= nbrvetement; i++)
    {
        int vetement = rand() % 20;
        int taille = rand() % 3;
        table_vetements[i].taille = taille;
        table_vetements[i].num = vetement;
    }

    // Recupere les vetements
    for (int i = 0; i <= nbrvetement; i++)
    {
        char semaphore_name[50];
        sprintf(semaphore_name, "/vetement_%d_%d", table_vetements[i].num, table_vetements[i].taille);
        sem_t *mySemaphore = sem_open(semaphore_name, 0);
        sem_trywait(mySemaphore);
        printf("prend vetement %d: num %d taille %d\n", i, table_vetements[i].num, table_vetements[i].taille);
    }

    // Rentre dans la file
    /*
    if (mkfifo(FIFO_NAME, 0666) == -1) {
        perror("mkfifo");
        exit(EXIT_FAILURE);
    }
    int attente = open(FIFO_NAME, O_WRONLY);
    if (attente == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }
    pid_t myPID = getpid();
    if (write(attente, &myPID, sizeof(pid_t)) == -1) {
        perror("write");
        exit(EXIT_FAILURE);
    }*/

    // Attend dans la fille
    printf("fais la queud\n");
    sleep(2);

    // Essaye les vetements
    printf("essaye les vetements\n");
    sleep(3);

    // Repose les vetements
    for (int i = 0; i <= nbrvetement; i++)
    {
        char semaphore_name[50];
        sprintf(semaphore_name, "/vetement_%d_%d", table_vetements[i].num, table_vetements[i].taille);
        sem_t *mySemaphore = sem_open(semaphore_name, 0);
        sem_post(mySemaphore);
        printf("libere vetement %d: num %d taille %d\n", i, table_vetements[i].num, table_vetements[i].taille);
    }

    return 0;
}

int main()
{
    client(2);

    return 0;
}