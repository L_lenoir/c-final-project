#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>

int main()
{
    int file;
    char *myfifo = "/tmp/file_attente";
    mkfifo(myfifo, 0666);

    file = open(myfifo, O_RDONLY);
    if (file == -1)
    {
        perror("Erreur lors de l'ouverture du fichier FIFO en lecture");
        exit(EXIT_FAILURE);
    }

    char buffer[256];
    while (1)
    {
        ssize_t bytesRead = read(file, buffer, sizeof(buffer) - 1);
        if (bytesRead == -1)
        {
            perror("Erreur lors de la lecture depuis le FIFO");
            exit(EXIT_FAILURE);
        }

        if (bytesRead > 0)
        {
            buffer[bytesRead] = '\0'; // Assurez-vous d'ajouter le caractère de fin de chaîne pour une utilisation avec printf
            printf("Message reçu du FIFO : %s\n", buffer);
        }

        sleep(1); // Ajout d'une pause pour éviter de saturer le système en lisant en continu
    }

    close(file);

    return 0;
}
