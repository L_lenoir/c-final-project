#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <string.h>

#define VETEMENT 20
#define TAILLE 3
#define NBR_VETEMENT 5

int creer_client()
{
    int file;
    char *myfifo = "/tmp/file_attente";
    mkfifo(myfifo, 0666);

    // créer la file nomé
    file = open(myfifo, O_RDONLY);
    if (file == -1)
    {
        perror("Erreur lors de l'ouverture du fichier FIFO en lecture");
        exit(EXIT_FAILURE);
    }

    char buffer[256];
    while (1)
    {
        // lis la file et touve un pid
        ssize_t bytesRead = read(file, buffer, sizeof(buffer) - 1);
        if (bytesRead == -1)
        {
            perror("Erreur lors de la lecture depuis le FIFO");
            exit(EXIT_FAILURE);
        }

        // envoie au client un signal
        if (bytesRead > 0)
        {
            buffer[bytesRead] = '\0';
            pid_t target_pid = buffer;
            if (kill(target_pid, SIGUSR1) == -1) {
                perror("kill");
                exit(EXIT_FAILURE);
            }
        }

        sleep(1);
    }

    close(file);

    return 0;
}

int main()
{

}
