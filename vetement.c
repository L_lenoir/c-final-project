#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <fcntl.h>

#define VETEMENT 20
#define TAILLE 3
#define NBR_VETEMENT 5

int main()
{
    sem_t *magasin[VETEMENT][TAILLE];

    for (int i = 0; i < VETEMENT; i++)
    {
        for (int j = 0; j < TAILLE; j++)
        {
            char semaphore_name[50];
            sprintf(semaphore_name, "/vetement_%d_%d", i, j);
            magasin[i][j] = sem_open(semaphore_name, O_CREAT | O_EXCL, 0666, NBR_VETEMENT);

            if (magasin[i][j] == SEM_FAILED)
            {
                perror("Erreur lors de la création de la sémaphore nommée");
                exit(EXIT_FAILURE);
            }
        }
    }
	return 0;
}
